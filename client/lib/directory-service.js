import { DirectoryServiceBase, ChainConnectionInfo } from 'ft3-lib';
import { blockchainRID, nodeApiUrl } from '../configs/constants';
const chainList = [

  // Our local chain
  new ChainConnectionInfo(
    Buffer.from(
      blockchainRID,
      'hex'
    ),
    nodeApiUrl
  )
];

export default class DirectoryService extends DirectoryServiceBase {
  constructor() {
    super(chainList);
  }
}